#ifndef __MONAT_H__
#define __MONAT_H__

#include "util.h"

/***************************************
* Aufgabe 1.6 - Header:
***************************************/

typedef struct month month_t;
struct month
{
  // Die Monatsnummerierungen k�nnen h�chstens zweistellig sein,
  // und sind immer positiv.
  uint_fast8_t number;
  // Die Anzahl der Tage des Monats kann h�chstens zweistellig sein,
  // und ist immer positiv.
  uint_fast8_t days;
  char *name;
};

void month_Ctor(month_t *month, int_fast16_t year, int_fast8_t month_number);

#endif
