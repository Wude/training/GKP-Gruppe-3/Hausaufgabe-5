#include "stdafx.h"

#include "datum_pointer.h"
#include "datum.h"

/***************************************
* Aufgabe 1.8 - Zusatzaufgabe: Datum mit Pointer (Implementation):
***************************************/

bool date2_IsValid(const date2_t *const date)
{
  if (
    (date->year < 1 || date->year > 9999) ||
    (date->month->number < 1 || date->month->number > 12) ||
    (date->day < 1))
  {
    return false;
  }
  switch (date->month->number)
  {
    case 2:
      return date->day <= (isLeapYear(date->year) ? 29 : 28);
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      return date->day <= 30;
      break;
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      return date->day <= 31;
      break;
  }
  return false;
}

cmp_t date2_Compare(const date2_t *const left, const date2_t *const right)
{
  if (left->year < right->year)
  {
    return CMP_LESS;
  }
  else if (left->year > right->year)
  {
    return CMP_GREATER;
  }
  if (left->month < right->month)
  {
    return CMP_LESS;
  }
  else if (left->month->number > right->month->number)
  {
    return CMP_GREATER;
  }
  if (left->day < right->day)
  {
    return CMP_LESS;
  }
  else if (left->day > right->day)
  {
    return CMP_GREATER;
  }
  return CMP_EQUAL;
}

bool date2_Equals(const date2_t *const left, const date2_t *const right)
{
  return
    (left->year == right->year) &&
    (left->month->number == right->month->number) &&
    (left->day == right->day);
}

uint_fast16_t date2_DayOfYear(const date2_t *const date)
{
  // Die Berechnung der Tagesnummer des Jahres wird durch die Verwendung von "month_t" nicht einfacher.
  // Die Verwendung der Arrays spart eine Schleife und ist auch noch schneller.
  return date->day +
    (isLeapYear(date->year) ? DAYS_TO_MONTH_366[date->month->number] :
                              DAYS_TO_MONTH_365[date->month->number]);
}

int_fast32_t date2_DiffDays(const date2_t *const left, const date2_t *const right)
{
  cmp_t cmp = date2_Compare(left, right);
  if (cmp == CMP_EQUAL)
  {
    return 0;
  }
  const date2_t *const lesser_date = (cmp == CMP_LESS ? left : right);
  const date2_t *const greater_date = (cmp == CMP_LESS ? right : left);
  int_fast32_t days;
  if (greater_date->year == lesser_date->year)
  {
    // Die abgelaufenen Jahrstage des gr��eren Datums
    // abz�glich derer des kleineren Datums.
    days = date2_DayOfYear(greater_date) - date2_DayOfYear(lesser_date);
  }
  else
  {
    // Die verbleibenden Jahrstage des kleineren Datums
    // zuz�glich der abgelaufenen Jahrstage des gr��eren Datums.
    days =
      (isLeapYear(lesser_date->year) ? 366 : 365) - date2_DayOfYear(lesser_date) +
      date2_DayOfYear(greater_date);
    // Die Tage der vollen zwischenliegenden Jahre aufaddieren.
    for (uint_fast16_t y = lesser_date->year + 1; y < greater_date->year; y++)
    {
      days += (isLeapYear(y) ? 366 : 365);
    }
  }
  return days * -cmp;
}

#define INPUT_SIZE 64

date2_t date2_GetFromConsole()
{
  date2_t date = { 0, MALLOC(month_t), 0 };
  char str[INPUT_SIZE];
  stringGetFromConsole(str, INPUT_SIZE, "Bitte ein Datum eingeben (kurzes oder langes Format):");
  _strlwr_s(str, INPUT_SIZE);
  int i = 0;
  char *found, *next;
  const char delimiter[] = ". ";
  found = strtok_s(str, delimiter, &next);
  while (found != NULL)
  {
    if (strlen(found) > 0)
    {
      switch (i)
      {
        case 0:
          date.day = atoi(found);
          break;
        case 1:
          if (strcmp(found, "jan") == 0 || strcmp(found, "januar") == 0)
          {
            month_Ctor(date.month, date.year, 1);
          }
          else if (strcmp(found, "feb") == 0 || strcmp(found, "februar") == 0)
          {
            month_Ctor(date.month, date.year, 2);
          }
          else if (strcmp(found, "mar") == 0 || strcmp(found, "maerz") == 0)
          {
            month_Ctor(date.month, date.year, 3);
          }
          else if (strcmp(found, "apr") == 0 || strcmp(found, "april") == 0)
          {
            month_Ctor(date.month, date.year, 4);
          }
          else if (strcmp(found, "mai") == 0)
          {
            month_Ctor(date.month, date.year, 5);
          }
          else if (strcmp(found, "jun") == 0 || strcmp(found, "juni") == 0)
          {
            month_Ctor(date.month, date.year, 6);
          }
          else if (strcmp(found, "jul") == 0 || strcmp(found, "juli") == 0)
          {
            month_Ctor(date.month, date.year, 7);
          }
          else if (strcmp(found, "aug") == 0 || strcmp(found, "august") == 0)
          {
            month_Ctor(date.month, date.year, 8);
          }
          else if (strcmp(found, "sep") == 0 || strcmp(found, "september") == 0)
          {
            month_Ctor(date.month, date.year, 9);
          }
          else if (strcmp(found, "okt") == 0 || strcmp(found, "oktober") == 0)
          {
            month_Ctor(date.month, date.year, 10);
          }
          else if (strcmp(found, "nov") == 0 || strcmp(found, "november") == 0)
          {
            month_Ctor(date.month, date.year, 11);
          }
          else if (strcmp(found, "dez") == 0 || strcmp(found, "dezember") == 0)
          {
            month_Ctor(date.month, date.year, 12);
          }
          else
          {
            month_Ctor(date.month, date.year, atoi(found));
          }
          break;
        case 2:
          date.year = atoi(found);
          break;
        default:
          // Error
          return (date2_t) { 0, date.month, 0 };
          break;
      }
      i++;
    }
    found = strtok_s(NULL, delimiter, &next);
  }
  return date;
}

void date2_PutToConsole(const date2_t *const date, bool long_representation)
{
  printf("%02d.", date->day);
  if (long_representation)
  {
    // Durch die Verwendung von "month_t" wird die Ausgabe des Monatsnamens einfacher.
    printf(" %s ", date->month->name);
  }
  else
  {
    printf("%02d.", date->month->number);
  }
  printf("%04d", date->year);
}

void date2_Dtor(date2_t *date)
{
  free(date->month);
  date->month = NULL;
}

void date2_Test()
{
  date2_t d1 = date2_GetFromConsole();
  date2_t d2 = date2_GetFromConsole();

  bool is_valid_1 = date2_IsValid(&d1);
  bool is_valid_2 = date2_IsValid(&d2);
  uint_fast16_t day_of_year_1 = date2_DayOfYear(&d1);
  uint_fast16_t day_of_year_2 = date2_DayOfYear(&d2);
  bool equals = date2_Equals(&d1, &d2);
  uint_fast32_t diff_days = date2_DiffDays(&d1, &d2);

  printf("\nDatum 1 (Wert) = ");
  date2_PutToConsole(&d1, false);
  printf("\nIst Datum 1 gueltig? = %s\n", (is_valid_1 ? "Ja" : "Nein"));
  printf("Datum 1 - Tag des Jahres = %ld\n", day_of_year_1);

  printf("\nDatum 2 (Wert) = ");
  date2_PutToConsole(&d2, true);
  printf("\nIst Datum 2 gueltig? = %s\n", (is_valid_2 ? "Ja" : "Nein"));
  printf("Datum 2 - Tag des Jahres = %ld\n", day_of_year_2);

  printf("\nSind die beiden Datumswerte gleich? = %s\n", (equals ? "Ja" : "Nein"));
  printf("Die Differenz der beiden Datumswerte in Tagen = %ld\n\n", diff_days);

  date2_Dtor(&d1);
  date2_Dtor(&d2);

  pause();
}
